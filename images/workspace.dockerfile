FROM archlinux
ARG USER=me
ENV USER=$USER
ENV NVIM_LOG_FILE=/dev/null

RUN { \
      echo "[mdaffin]"; \
      echo "SigLevel = Optional TrustAll"; \
      echo "Server = https://s3.eu-west-2.amazonaws.com/mdaffin-arch/repo/x86_64"; \
    } >> /etc/pacman.conf

RUN pacman -Syu --noconfirm --noprogress --needed \
      base-devel \
      zsh grml-zsh-config \
      exa ripgrep fzf fd bat watchexec \
      neovim-git python-msgpack python-pynvim clang \
      git openssh \
      xsel \
      docker docker-compose kind kubectl \
      nodejs node-gyp \
      rustup rust-analyzer musl \
      bash-language-server gopls dockerfile-language-server-bin \
      vscode-css-languageserver-bin vscode-html-languageserver-bin \
      vscode-json-languageserver-bin python-language-server \
      terraform-ls typescript-language-server-bin yaml-language-server \
      vim-language-server \
    && rm -rf /var/cache/pacman/pkg/*

RUN ln -sf /usr/bin/exa /usr/bin/ls
RUN ln -sf /usr/bin/nvim /usr/bin/vim
RUN ln -sf /usr/bin/nvim /usr/bin/vi

RUN useradd --create-home --user-group --shell=/bin/zsh --groups=docker "$USER"
RUN echo "$USER ALL=(ALL) NOPASSWD: ALL" > "/etc/sudoers.d/$USER"

USER $USER

RUN rustup toolchain install stable
RUN rustup target install x86_64-unknown-linux-musl
RUN rustup component add rust-src

USER root

COPY workspace/usr/bin /usr/bin

RUN add-nvim-plugin opt neovim/nvim-lspconfig
RUN add-nvim-plugin start liuchengxu/vim-which-key
RUN add-nvim-plugin start ap/vim-buftabline
RUN add-nvim-plugin start ekalinin/Dockerfile.vim
RUN add-nvim-plugin opt dracula/vim dracula
RUN add-nvim-plugin opt nvim-lua/lsp_extensions.nvim
RUN add-nvim-plugin opt nvim-lua/completion-nvim
RUN add-nvim-plugin opt nvim-lua/lsp-status.nvim
RUN add-nvim-plugin opt junegunn/fzf.vim

ENV DOCKER_HOST=tcp://localhost:2376
ENV DOCKER_TLS_VERIFY=1

COPY workspace/ /

USER $USER

CMD ["zsh", "-l"]
