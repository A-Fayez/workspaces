local lsp_status = require('lsp-status')
local completion = require('completion')

lsp_status.register_progress()

local map = function(type, key, value)
	vim.fn.nvim_buf_set_keymap(0,type,key,value,{noremap = true, silent = true});
end

local on_attach = function(client, bufnr)
  completion.on_attach(client, bufnr)
  lsp_status.on_attach(client, bufnr)

  vim.api.nvim_command('autocmd BufWritePre *.rs lua vim.lsp.buf.formatting_sync(nil, 500)')
  vim.api.nvim_command('autocmd CursorHold,CursorHoldI,CursorMoved *.rs :lua require\'lsp_extensions\'.inlay_hints{ prefix = " » ", highlight = "Comment", aligned = true, enabled = {"TypeHint", "ChainingHint", "ParameterHint"} }')

  map('n','gD','<cmd>lua vim.lsp.buf.declaration()<CR>')
  map('n','gd','<cmd>lua vim.lsp.buf.definition()<CR>')
  map('n','K','<cmd>lua vim.lsp.buf.hover()<CR>')
  map('n','gr','<cmd>lua vim.lsp.buf.references()<CR>')
  map('n','gs','<cmd>lua vim.lsp.buf.signature_help()<CR>')
  map('n','gi','<cmd>lua vim.lsp.buf.implementation()<CR>')
  map('n','gt','<cmd>lua vim.lsp.buf.type_definition()<CR>')
  map('n','<leader>gw','<cmd>lua vim.lsp.buf.document_symbol()<CR>')
  map('n','<leader>gW','<cmd>lua vim.lsp.buf.workspace_symbol()<CR>')
  map('n','<leader>ah','<cmd>lua vim.lsp.buf.hover()<CR>')
  map('n','<leader>af','<cmd>lua vim.lsp.buf.code_action()<CR>')
  map('n','<leader>ee','<cmd>lua vim.lsp.util.show_line_diagnostics()<CR>')
  map('n','<leader>ar','<cmd>lua vim.lsp.buf.rename()<CR>')
  map('n','<leader>=', '<cmd>lua vim.lsp.buf.formatting()<CR>')
  map('n','<leader>ai','<cmd>lua vim.lsp.buf.incoming_calls()<CR>')
  map('n','<leader>ao','<cmd>lua vim.lsp.buf.outgoing_calls()<CR>')
end

require'lspconfig'.bashls.setup({
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
})
require'lspconfig'.cssls.setup({
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
})
require'lspconfig'.dockerls.setup({
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
})
require'lspconfig'.gopls.setup({
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
})
require'lspconfig'.html.setup({
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
})
require'lspconfig'.jsonls.setup({
  settings = {
    cmd = { "json-languageserver", "--stdio" }
  },
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
})
require'lspconfig'.pyls.setup({
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
})
require'lspconfig'.rust_analyzer.setup({
  on_attach = on_attach,
  capabilities = lsp_status.capabilities,
  settings = {
    ["rust-analyzer"] = { checkOnSave = { enable = false } }
  }
})
require'lspconfig'.terraformls.setup({
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
})
require'lspconfig'.tsserver.setup({
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
})
require'lspconfig'.vimls.setup({
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
})
require'lspconfig'.yamlls.setup({
  on_attach = on_attach,
  capabilities = lsp_status.capabilities
})

