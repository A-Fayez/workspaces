packadd dracula
if (has("termguicolors"))
 set termguicolors
endif
syntax enable
colorscheme dracula

packadd lsp-status.nvim
function! LspStatus() abort
  if luaeval('#vim.lsp.buf_get_clients() > 0')
    return luaeval("require('lsp-status').status()")
  endif
  return ''
endfunction

set statusline+=\ %{LspStatus()}`

packadd completion-nvim
let g:completion_confirm_key = "\<C-y>"
set completeopt=menuone,noinsert,noselect
set shortmess+=c
let g:completion_enable_snippet = 'UltiSnips'

packadd diagnostic-nvim
let g:diagnostic_insert_delay = 1
let g:diagnostic_show_sign = 1
let g:diagnostic_enable_virtual_text = 1

packadd lsp_extensions.nvim
packadd nvim-lspconfig
lua require("lsp")

packadd fzf.vim

filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab

set list
set listchars=tab:▸\ ,extends:❯,precedes:❮,trail:·,nbsp:·,space:·
set colorcolumn=+1
set number
set nocursorcolumn
set lazyredraw
set splitbelow
set splitright
autocmd FileType help wincmd L

set hidden
set mouse=a

nnoremap <silent> <C-p> :Files<CR>
nnoremap <silent> <C-f> :Rg<CR>
vnoremap <silent> <C-c> "+y
inoremap <silent> <C-v> <ESC>"+pa
