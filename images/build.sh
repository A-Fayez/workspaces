#!/usr/bin/env bash
set -euo pipefail
# shellcheck disable=SC2154
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
IFS=$'\n\t'

cd "$(dirname "${BASH_SOURCE[0]}")"

docker build --tag "registry.gitlab.com/mdaffin/workspaces" --file "workspace.dockerfile" .
